package ru.renessans.jvschool.volkov.task.manager.api.configuration;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.endpoint.*;

import java.util.concurrent.Executor;

public interface IApplicationConfiguration {

    Executor executor();

    AuthenticationEndpointService authenticationEndpointService();

    AdminEndpointService adminEndpointService();

    AdminDataInterChangeEndpointService adminDataInterChangeEndpointService();

    SessionEndpointService sessionEndpointService();

    UserEndpointService userEndpointService();

    ProjectEndpointService projectEndpointService();

    TaskEndpointService taskEndpointService();

    AuthenticationEndpoint authenticationEndpoint(
            @NotNull AuthenticationEndpointService authenticationEndpointService
    );

    AdminEndpoint adminEndpoint(
            @NotNull AdminEndpointService adminEndpointService
    );

    AdminDataInterChangeEndpoint adminDataInterChangeEndpoint(
            @NotNull AdminDataInterChangeEndpointService adminDataInterChangeEndpointService
    );

    SessionEndpoint sessionEndpoint(
            @NotNull SessionEndpointService sessionEndpointService
    );

    UserEndpoint userEndpoint(
            @NotNull UserEndpointService userEndpointService
    );

    ProjectEndpoint projectEndpoint(
            @NotNull ProjectEndpointService projectEndpointService
    );

    TaskEndpoint taskEndpoint(
            @NotNull TaskEndpointService taskEndpointService
    );

}