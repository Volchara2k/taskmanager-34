package ru.renessans.jvschool.volkov.task.manager.listener.task;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskEndpoint;

@RequiredArgsConstructor
public abstract class AbstractTaskListener extends AbstractListener {

    @NotNull
    protected final TaskEndpoint taskEndpoint;

    @NotNull
    protected final ICurrentSessionService currentSessionService;

}