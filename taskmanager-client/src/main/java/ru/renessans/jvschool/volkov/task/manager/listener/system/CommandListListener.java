package ru.renessans.jvschool.volkov.task.manager.listener.system;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import java.util.Collection;

@Component
@RequiredArgsConstructor
public class CommandListListener extends AbstractListener {

    @NotNull
    private static final String CMD_COMMANDS = "commands";

    @NotNull
    private static final String ARG_COMMANDS = "-cmd";

    @NotNull
    private static final String DESC_COMMANDS = "вывод списка поддерживаемых терминальных команд";

    @NotNull
    private static final String NOTIFY_COMMANDS = "Список поддерживаемых терминальных команд: \n";

    @NotNull
    private final ICommandService commandService;

    @NotNull
    @Override
    public String command() {
        return CMD_COMMANDS;
    }

    @NotNull
    @Override
    public String argument() {
        return ARG_COMMANDS;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_COMMANDS;
    }

    @Async
    @Override
    @EventListener(condition = "@commandListListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        ViewUtil.print(NOTIFY_COMMANDS);
        @Nullable final Collection<String> terminalCommands = this.commandService.getAllTerminalCommands();
        ViewUtil.print(terminalCommands);
    }

}