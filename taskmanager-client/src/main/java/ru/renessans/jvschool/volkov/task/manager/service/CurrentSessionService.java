package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.repository.ICurrentSessionRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidSessionException;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public final class CurrentSessionService implements ICurrentSessionService {

    @NotNull
    private final ICurrentSessionRepository currentSessionRepository;

    @Nullable
    @SneakyThrows
    @Override
    public SessionDTO getCurrentSession() {
        return this.currentSessionRepository.get();
    }

    @NotNull
    @SneakyThrows
    @Override
    public SessionDTO subscribe(
            @Nullable final SessionDTO sessionDTO
    ) {
        if (Objects.isNull(sessionDTO)) throw new InvalidSessionException();
        return this.currentSessionRepository.set(sessionDTO);
    }

    @NotNull
    @SneakyThrows
    @Override
    public SessionDTO unsubscribe() {
        return this.currentSessionRepository.delete();
    }

}