package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IPropertyService;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;

public final class ConfigurationServiceTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConfigurationService CONFIG_SERVICE = new ConfigurationService(PROPERTY_SERVICE);

    @Before
    public void loadConfigurationBefore() {
        Assert.assertNotNull(PROPERTY_SERVICE);
        Assert.assertNotNull(CONFIG_SERVICE);
        PROPERTY_SERVICE.load();
    }

    @Test
    @TestCaseName("Run testGetServerHost for getServerHost()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetServerHost() {
        Assert.assertNotNull(CONFIG_SERVICE);
        @NotNull final String host = CONFIG_SERVICE.getServerHost();
        Assert.assertNotNull(host);
    }

    @Test
    @TestCaseName("Run testGetServerPort for getServerPort()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetServerPort() {
        Assert.assertNotNull(CONFIG_SERVICE);
        @NotNull final Integer port = CONFIG_SERVICE.getServerPort();
        Assert.assertNotNull(port);
    }

    @Test
    @TestCaseName("Run testGetSessionSalt for getSessionSalt()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetSessionSalt() {
        Assert.assertNotNull(CONFIG_SERVICE);
        @NotNull final String salt = CONFIG_SERVICE.getSessionSalt();
        Assert.assertNotNull(salt);
    }

    @Test
    @TestCaseName("Run testGetSessionCycle for getSessionCycle()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetSessionCycle() {
        Assert.assertNotNull(CONFIG_SERVICE);
        @NotNull final Integer cycle = CONFIG_SERVICE.getSessionCycle();
        Assert.assertNotNull(cycle);
    }

    @Test
    @TestCaseName("Run testGetBinFileName for getBinFileName()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetBinFileName() {
        Assert.assertNotNull(CONFIG_SERVICE);
        @NotNull final String binFileName = CONFIG_SERVICE.getBinPathname();
        Assert.assertNotNull(binFileName);
    }

    @Test
    @TestCaseName("Run testGetBase64FileName for getBase64FileName()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetBase64FileName() {
        Assert.assertNotNull(CONFIG_SERVICE);
        @NotNull final String base64FileName = CONFIG_SERVICE.getBase64Pathname();
        Assert.assertNotNull(base64FileName);
    }

    @Test
    @TestCaseName("Run testGetJsonFileName for getJsonFileName()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetJsonFileName() {
        Assert.assertNotNull(CONFIG_SERVICE);
        @NotNull final String jsonFileName = CONFIG_SERVICE.getJsonPathname();
        Assert.assertNotNull(jsonFileName);
    }

    @Test
    @TestCaseName("Run testGetXmlFileName for getXmlFileName()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetXmlFileName() {
        Assert.assertNotNull(CONFIG_SERVICE);
        @NotNull final String xmlFileName = CONFIG_SERVICE.getXmlPathname();
        Assert.assertNotNull(xmlFileName);
    }

    @Test
    @TestCaseName("Run testGetYamlFileName for getYamlFileName()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetYamlFileName() {
        Assert.assertNotNull(CONFIG_SERVICE);
        @NotNull final String yamlFileName = CONFIG_SERVICE.getYamlPathname();
        Assert.assertNotNull(yamlFileName);
    }

}