package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import ru.renessans.jvschool.volkov.task.manager.model.Session;

public interface ISessionRepository extends IRepository<Session> {

    @Nullable
    @Query("FROM Session WHERE user.id = ?1")
    Session getSessionByUserId(@NotNull String userId);

}