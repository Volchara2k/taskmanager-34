package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.model.User;

public interface IUserRepository extends IRepository<User> {

    @Modifying
    @Query("DELETE User WHERE login = ?1")
    int deleteUserByLogin(@NotNull String login);

    @Nullable
    @Query("FROM User WHERE login = ?1")
    User getByLogin(@NotNull String login);

}
