package ru.renessans.jvschool.volkov.task.manager.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlType;

@Data
@XmlType
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public final class SessionDTO extends AbstractDTO {

    private static final long serialVersionUID = 1L;

    @Nullable
    private Long timestamp = 0L;

    @Nullable
    private String userId;

    @Nullable
    private String signature = "";

}