package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.repository.IOwnerUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IOwnerUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalIndexException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTaskException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractUserOwner;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.*;

@Transactional
public abstract class AbstractUserOwnerService<E extends AbstractUserOwner> extends AbstractService<E> implements IOwnerUserService<E> {

    private final IOwnerUserRepository<E> ownerUserRepository;

    protected AbstractUserOwnerService(@NotNull final IOwnerUserRepository<E> ownerUserRepository) {
        super(ownerUserRepository);
        this.ownerUserRepository = ownerUserRepository;
    }

    @NotNull
    @SneakyThrows
    @Override
    public E updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String newTitle,
            @Nullable final String newDescription
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        if (ValidRuleUtil.isNullOrEmpty(newTitle)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(newDescription)) throw new InvalidDescriptionException();

        @Nullable final E value = this.getByIndex(userId, index);
        if (Objects.isNull(value)) throw new InvalidTaskException();
        value.setTitle(newTitle);
        value.setDescription(newDescription);

        return super.save(value);
    }

    @NotNull
    @SneakyThrows
    @Override
    public E updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String newTitle,
            @Nullable final String newDescription
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidIdException();
        if (ValidRuleUtil.isNullOrEmpty(newTitle)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(newDescription)) throw new InvalidDescriptionException();

        @Nullable final E value = this.getById(userId, id);
        if (Objects.isNull(value)) throw new InvalidTaskException();
        value.setTitle(newTitle);
        value.setDescription(newDescription);

        return super.save(value);
    }

    @SneakyThrows
    @Override
    public int deleteById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidIdException();
        return this.ownerUserRepository.deleteById(userId, id);
    }

    @SneakyThrows
    @Override
    public int deleteByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        return this.ownerUserRepository.deleteByTitle(userId, title);
    }

    @SneakyThrows
    @Override
    public int deleteAll(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        return this.ownerUserRepository.deleteAll(userId);
    }

    @SneakyThrows
    @Override
    public int deleteByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        @NotNull final List<E> values = this.getAll(userId);
        if (index >= values.size()) return 0;
        @Nullable E value = values.get(index);
        if (Objects.isNull(value)) return 0;
        return super.deleteRecordById(value.getId());
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public E getByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        @NotNull final List<E> values = this.getAll(userId);
        if (index >= values.size()) return null;
        return values.get(index);
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public E getById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidIdException();
        return this.ownerUserRepository.getById(userId, id);
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public E getByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        return this.ownerUserRepository.getByTitle(userId, title);
    }

    @NotNull
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public List<E> getAll(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        return this.ownerUserRepository.getAll(userId);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<E> initialDemoData(
            @Nullable final Collection<User> users
    ) {
        if (Objects.isNull(users)) throw new InvalidUserException();

        @NotNull final List<E> demoData = new ArrayList<>();
        users.forEach(user -> {
            @NotNull final Collection<E> getDataForUser = this.getAll(user.getId());
            if (ValidRuleUtil.isNullOrEmpty(getDataForUser)) {
                @NotNull final E data = add(user.getId(), UUID.randomUUID().toString(), UUID.randomUUID().toString());
                demoData.add(data);
            }
        });

        return demoData;
    }

}