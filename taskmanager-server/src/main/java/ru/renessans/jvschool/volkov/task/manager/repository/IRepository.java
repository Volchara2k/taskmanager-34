package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;

@NoRepositoryBean
public interface IRepository<E extends AbstractModel> extends JpaRepository<E, String> {

    @NotNull
    @Query("FROM #{#entityName} WHERE id = ?1")
    E getRecordById(@NotNull String id);

    @Modifying
    @Query("DELETE #{#entityName} WHERE id = ?1")
    int deleteRecordById(@NotNull String id);

    @Modifying
    @Query("DELETE #{#entityName}")
    int deleteAllRecords();

}
