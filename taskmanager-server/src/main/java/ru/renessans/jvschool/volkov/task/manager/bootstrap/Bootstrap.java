package ru.renessans.jvschool.volkov.task.manager.bootstrap;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.IEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.util.EndpointUtil;

import javax.annotation.PostConstruct;
import java.util.Collection;

@Setter
@Component
@RequiredArgsConstructor
public final class Bootstrap {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ITaskUserService taskUserService;

    @NotNull
    private final IProjectUserService projectUserService;

    @NotNull
    private final IConfigurationService configurationService;

    @NotNull
    private final Collection<IEndpoint> endpoints;

    public void run() {
        writeDemoDataIfNotExists();
    }

    private void writeDemoDataIfNotExists() {
        @NotNull final Collection<User> users = this.userService.initialDemoData();
        this.taskUserService.initialDemoData(users);
        this.projectUserService.initialDemoData(users);
    }

    @PostConstruct
    private void publishWebServices() {
        @Nullable final String host = this.configurationService.getServerHost();
        @Nullable final Integer port = this.configurationService.getServerPort();
        EndpointUtil.publish(this.endpoints, host, port);
    }

}