package ru.renessans.jvschool.volkov.task.manager.api.service.adapter;

import ru.renessans.jvschool.volkov.task.manager.dto.UserLimitedDTO;

public interface IUserLimitedAdapterService extends IUserAdapterService<UserLimitedDTO> {
}