package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IService;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.util.Collection;

public interface IOwnerUserService<E extends AbstractModel> extends IService<E> {

    @NotNull
    E add(
            @Nullable String userId,
            @Nullable String title,
            @Nullable String description
    );

    @NotNull
    E updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String newTitle,
            @Nullable String newDescription
    );

    @NotNull
    E updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String newTitle,
            @Nullable String newDescription
    );

    int deleteByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    int deleteById(
            @Nullable String userId,
            @Nullable String id
    );

    int deleteByTitle(
            @Nullable String userId,
            @Nullable String title
    );

    int deleteAll(
            @Nullable String userId
    );

    @Nullable
    E getByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @Nullable
    E getById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    E getByTitle(
            @Nullable String userId,
            @Nullable String title
    );

    @NotNull
    Collection<E> getAll(
            @Nullable String userId
    );

    @NotNull
    Collection<E> initialDemoData(
            @Nullable Collection<User> users
    );

}