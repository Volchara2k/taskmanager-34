package ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidTaskException extends AbstractException {

    @NotNull
    private static final String EMPTY_TASK = "Ошибка! Параметр \"задача\" отсутствует!\n";

    public InvalidTaskException() {
        super(EMPTY_TASK);
    }

}