package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;

import java.util.List;

@NoRepositoryBean
public interface IOwnerUserRepository<E extends AbstractModel> extends IRepository<E> {

    @Modifying
    @Query("DELETE #{#entityName} WHERE user.id = ?1 AND id = ?2")
    int deleteById(
            @NotNull String userId,
            @NotNull String id
    );

    @Modifying
    @Query("DELETE #{#entityName} WHERE user.id = ?1 AND title = ?2")
    int deleteByTitle(
            @NotNull String userId,
            @NotNull String title
    );

    @Modifying
    @Query("DELETE #{#entityName} WHERE user.id = ?1")
    int deleteAll(
            @NotNull String userId
    );

    @NotNull
    @Query("FROM #{#entityName} WHERE user.id = ?1 ORDER BY creationDate")
    List<E> getAll(
            @NotNull String userId
    );

    @Nullable
    @Query("FROM #{#entityName} WHERE user.id = ?1 AND id = ?2 ORDER BY creationDate")
    E getById(
            @NotNull String userId,
            @NotNull String id
    );

    @Nullable
    @Query("FROM #{#entityName} WHERE user.id = ?1 AND title = ?2 ORDER BY creationDate")
    E getByTitle(
            @NotNull String userId,
            @NotNull String title
    );

}
